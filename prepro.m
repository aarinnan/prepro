function [xpp, ppset] = prepro( x, met, opt)
%[xpp, ppset] = prepro( x, met, opt)
%
% Pre-processing of spectra. So far the same pre-processing is used on the
% whole spectra
%
%INPUT:
% x     Spectral data with samples as rows
% met   Method - SNV, MSC, SG (Savitzky-Golay), MC (mean-centering), AS
%        (autoscaling) and PS (Pareto-scaling)
% opt   Settings for the pre-processing wanted. Number of inputs depends on
%        method
%
%OUTPUT:
% xpp   Pre-processed spectra
% ppset Values needed for pre-processing, i.e. mean( xcal) for MSC
%
%See also: MSCT, SNV, SG

% 130313 AAR Added the correction parameters from MSC/ SNV as an output
% 070110 AAR

if nargin == 0
    xpp = struct( 'ref', NaN, 'ax', NaN, 'set', NaN);
    return
end

if nargin < 3
    error( '''opt'' must be given even if it is empty (as for SNV)')
end

if ~isstruct( opt)
    opt = prepro;
end

ppset = opt;

met = lower( met);
if sum( strcmp( met, {'snv', 'msc', 'sg', 'mc', 'as', 'ps', 'specdet'}) ) == 0
    error( 'Method not recognized. Choose between snv, msc, sg, mc, as, ps and specdet')
end

if iscell( met)
    if length( met) > 1
        disp( 'It will only perform the first pre-processing on your list, run several times to perform multiple pre-processing steps.')
    end
    met = met{1};    
end

switch met
    case 'snv'
        xpp = snv( x); %Could be enhanced with including MAD/ Quartile range, etc
        ppset.B = [ nanmean( x, 2) nanstd( x, 2)];
    case 'msc'
        if isnan( opt.ref)
            opt.ref = mean( x);
        end
        if isnan( opt.ax)
            opt.ax = 1:size( x, 2);
        end
        if isnan( opt.set)
            opt.set = [1 0 0]; %MSC
        end
        [xpp, xw, xref, xwav, b] = msct( x, opt.ref, opt.ax, opt.set(1), opt.set(2), NaN, opt.set(3));
        ppset.ref = opt.ref;
        ppset.ax = opt.ax;
        ppset.B = b;
    case 'sg'
        xpp = sg( x, opt.set(1), opt.set(2), opt.set(3), 0);
    case 'mc'
        if isnan( opt.ref)
            [xpp, ppset.ref] = cen_std( x);
        else
            xpp = cen_std( x, opt.ref);
        end
    case 'as'
        if isnan( opt.ref)
            [xpp, xm, xs] = cen_std( x, 'a');
            ppset.ref = [xm; xs];
        else
            xpp = cen_std( x, opt.ref( 1, :), opt.ref( 2, :) );
        end
    case 'ps'
        if isnan( opt.ref)
            ppset.ref = [nanmean( x); sqrt( nanstd( x) )];
            xpp = cen_std( x, ppset.ref( 1, :), ppset.ref( 2, :) );
        else
            xpp = cen_std( x, opt.ref( 1, :), opt.ref( 2, :) );
        end
    case 'specdet'
        if isnan( opt.ax)
            opt.ax = 1:size( x, 2);
        end
        if isnan( opt.set)
            opt.set = [2 0 0]; %Fitting 2nd order polynomial
        end
        xpp = specdet( x, opt.ax, opt.set(1) );
end