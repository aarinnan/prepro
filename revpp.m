function xp = revpp( T, P, met, ppset)
%xp = revpp( T, P, met, ppset)
%
%INPUT:
% T      Scores
% P      Loadings
% met    PP methods (as in the input to 'prepro'
% ppset  Output from 'prepro'
%
%OUTPUT:
% xp     The predicted raw data

%300915 AAR

r = size( T, 1);
c = size( P, 1);

xp = T * P';
for cm = length( met):-1:1
    switch lower( met{cm})
        case { 'mc'}
            xp = xp + ppset{cm}.ref( ones( r, 1), :);
        case { 'as'}
            xp = xp .* ppset{cm}.ref( ones( r, 1) * 2, :) + ppset{cm}.ref( ones( r, 1), :);
        case { 'msc'}
%             xraw = b0 + b1 * xref + error
%             xcorr = ( xraw - b0)/ b1;
%             xraw = xcorr * b1 + b0
            xp = xp .* ppset{cm}.B( :, ones( c, 1) * 2) + ppset{ cm}.B( :, ones( c, 1));
        case { 'snv'}
%             xcorr = ( xraw - mean( xraw))/ std( xraw);
%             xraw = xcorr * std( xraw) + mean( xraw);
            xp = xp .* ppset{cm}.B( :, ones( c, 1) * 2) + ppset{ cm}.B( :, ones( c, 1));
        case { 'sg'}
    end
end