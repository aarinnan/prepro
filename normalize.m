function [ a, nor] = normalize( x, mode, opt)
%[ a, nor] = normalize( x, mode, opt)
%
% Normalized either the columns or rows of x
%
% INPUT
%  x     The columns/ rows to be normalized
%  mode  1 equals columns (default), 2 equals rows
%  opt   2 equals 2. norm (default), 1 equals 1. norm, 0 equals scaled
%         between 0 and 1
%
% OUTPUT
%  a     The normalized columns/rows
%  nor   The norm

% 200819 AAR Added scaling between 0 and 1
% 030113 AAR Added 1. norm as well as 2. norm

if nargin < 2
    mode = 1;
end
if mode == 2 || size( x, 1) == 1
    x = x';
end

%Got an error with logical matrices
if islogical( x)
    x = double( x);
end

c = isnan( x);
x( c) = 0;
if nargin < 3
    opt = 2;
end

switch opt
    case 0
        nor = range( x);
    case 1
        nor = sum( abs( x) );
    case 2
        nor = sqrt( sum( x.*x) );
    otherwise
        error( 'This value of ''opt'' is not possible')
end
if opt == 0
    a = ( x - min( x))./ nor;
else
    a = x ./ ( ones( size( x, 1), 1) * nor);
end

if mode == 2
    a = a';
end
a( isnan( c)) = NaN;