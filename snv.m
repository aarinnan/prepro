function xnew=snv(x,ax,opt)
% Standard Normal Variate scaling of x
% xnew=snv(x,ax,opt)
%
%INPUT:
% x     X-matrix to be transformed
% ax    Axis. If you want windowed SNV, give the axis divided into cells
%        Default = 1:<size(x,2)>
% opt   [<Robust> <Windowsize>]
%        Robust = 1 : Median and MAD for correction instead of mean and std
%        Windowsize : 0 indicate normal, else moving window SNV
%
%OUTPUT:
% xnew  SNV corrected spectra
%
% See also: SG, MSC, EMSC, BASELINE, SPECDET

%070613 AAR Improved the WSNV functionality
%190907 AAR Added robust measures of mean and standard deviation
%150707 AAR Added the possibility to perform WSNV and PSNV
%191206 AAR

[r,c]=size(x);
if nargin<2 || isempty(ax)
    ax=1:c;
end
if nargin<3 || isempty(opt)
    opt=[0 0];
end
if ~iscell(ax)
    ax={ax};
end
if opt(2)==0
    ind=[];
    for i=1:length(ax) %Piecewise SNV, if length(ax)==1 -> normal SNV
        ind=[ind;ones(length(ax{i}),1)*i];
        xtemp=x(:,ind==i);
        if opt(1)~=1
            temp=[nanmean(xtemp');nanstd(xtemp')]';
        else
            temp=[median(xtemp');mad(xtemp')]';
        end
        xnew(:,ind==i)=(xtemp-temp(:,1)*ones(1,sum(ind==i)))./(temp(:,2)*ones(1,sum(ind==i)));
    end
else %Moving window SNV
    for i=1:c%-opt(2)*2)
        lim = [max( i - opt(2), 1) min( i + opt(2), c)];
        if i < opt(2)
            lim(3) = i;
        else
            lim(3) = opt(2) + 1;
        end
        xtemp=x(:,lim(1):lim(2));
        if opt(1)~=1
            temp=[nanmean(xtemp');nanstd(xtemp')]';
        else
            temp=[median(xtemp');mad(xtemp')]';
        end
        
        xnew( :, i) = (xtemp( :, lim(3) ) - temp( :, 1) )./ temp( :, 2);
    end
end