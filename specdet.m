function [xnew, b] = specdet(x,ax,n)
%Detrending of spectra using a n-polynomial
% [xnew, b] = specdet(x,ax,n)
%
%INPUT:
% x     Raw spectra. Samples in rows
% ax    Wavelength axes. (Could also be given in cellformat, partial detrend)
% n     Degree of polynomial. n > 0 <default = 2>
%
%OUTPUT:
% xnew  Detrended spectra
% b     The offset and slope used in the corrections
%
% See also: MSC, SG, SNV

% 101213 AAR Included the B in the output
% 280307 AAR Any polynomial is allowed. Made sure that the polynomial is an
%             integer
% 110107 AAR

%Set default value
if nargin<3
    n=2;
end
if nargin<2 || isempty(ax)
    ax{1}=1:size(x,2);
end
if n<1
    error('''n'' has been set to a non-valid number. n = 1:4')
end

n=ceil(n); %Make sure n is an integer

[r,c]=size(x);
if ~iscell(ax)
    temp{1}=ax;
    clear ax
    ax=temp;
end
id=[];
for i=1:length(ax)
    id=[id;ones(length(ax{i}),1)*i];
end

%Do the detrending
for i=1:length(ax)
    xtemp=x(:,id==i);
    temp=zeros(c,n+1);
    temp(:,1)=ones(length(ax{i}),1);
    if n>0
        for j=1:n
            temp(:,j+1)=vec(ax{i}).^j;
        end
    end
    tinv=pinv(temp,3*eps);
    b{i}=(tinv*xtemp')';
    xnew(:,id==i)=xtemp-b{i}*temp';
%     for j=1:r
%         b=tinv*xtemp(j,:)';
%         xnew(j,id==i)=xtemp(j,:)-(temp*b)';
%     end
end

if length( ax) == 1
    temp = b{1};
    clear b
    b = temp;
end